/*
Código introductorio a los arreglos
*/

#include <iostream>

using namespace std;

int main(){

//Vamos a tomar como ejemplo la calificacion que le dan 5 jueces
//a clavadistas

//Declaración e inicialización de cada vector.

float califA[5]= {2.2,5.5,6.0,5.8,6};
float promedioA=0;
float califB[5]= {8.5,7.0,6.9,8.3,9};
float acumuladorB=0;
float promedioB=0;
float califC[5]= {10,10,10,8,9};


//Muestro por pantalla los resultados:
cout<<"Calificaciones clavadista A:"<<endl;
	for(int i=0; i<5; i++){
	cout<<" "<<califA[i];
	promedioA+=califA[i];
	}
promedioA=promedioA/5;
cout<<endl<<"Promedio clavadista A: "<<promedioA<<endl;
cout<<endl<<endl;

cout<<"Calificaciones clavadista B:"<<endl;
	for(int i=0; i<5; i++){
	cout<<" "<<califB[i];
	acumuladorB+=califB[i];
	}
promedioB=acumuladorB/5;
cout<<endl<<"Promedio clavadista B: "<<promedioB<<endl;
cout<<endl<<endl;


cout<<"Calificaciones clavadista C:"<<endl;
	for(int i=0; i<5; i++){
	cout<<" "<<califC[i];
	}
cout<<endl<<endl;


//Recordar que los indices empiezan en CERO. Por lo que uno puede reccorrer
//un arreglo hasta un indice como maximo dimension-1

/*
¿como haria para que en vez de inicializar los arreglos con los datos, se
carguen por teclado?
¿Como sacaria el promedio de cada clavadista?
*/

return 0;
}