#include <iostream>
#include <cmath>

using namespace std;

const double PI = 3.14159265358979323846;

double volumenEsfera(float);

int main(){

	float r=0;
	double vol=0;
	cout<<"Ingrese radio: [m]"<<endl;
	cin>>r;

	vol = volumenEsfera(r);

	cout<<"El volumen es= "<<vol<<" m3"<<endl;

	cin.get();

	return 0;
}

/*
V=4/3 * pi*r^3
*/
double volumenEsfera(float r){

	//double pow (double base, double exponent);

	double volumen=(4/3)*PI*pow(r,3);

	return volumen;

}