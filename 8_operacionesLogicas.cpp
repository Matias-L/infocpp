#include <iostream>

using namespace std;

int main(){

bool A=false;
bool B=true;


cout<<endl<<"Valores iniciales: "<<endl;
cout<<"A="<<A<<endl;
cout<<"B="<<B<<endl;

cout<<"-------"<<endl;


cout<<"A="<<A<<endl;
cout<<"Not A="<<not A<<endl;
cout<<"Not not A="<<not not A;

cout<<endl<<endl<<"------"<<endl;

/*
OJO PARENTESIS!!
mostrar error
*/

cout<<"A="<<A<<endl;
cout<<"B="<<B<<endl;
cout<<"A and B= "<<(A and B)<<endl;
cout<<"A and A= "<<(A && A)<<endl;
cout<<"B and B= "<<(B && B)<<endl;


cout<<endl<<endl<<"------"<<endl;

cout<<"A="<<A<<endl;
cout<<"B="<<B<<endl;
cout<<"A or B= "<<(A or B)<<endl;
cout<<"A or (not B)= "<< (A || !B)<<endl;



return 0;
}