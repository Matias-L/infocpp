#include <iostream>
#include <cmath>
using namespace std;
/*
Usamos pasaje por referencias
*/

void mensajeBienvenida(void);
void baskara(double, double, double, double&, double&);


int main(){

	mensajeBienvenida();	//Muestra un mensaje simple

	double x1=0; 
	double x2=0;
	double a=2;
	double b=4;
	double c=1;

	cout<<"Ingrese coeficientes: "<<endl;
	cout<<"a="<<endl;
	cin>>a;
	cout<<"b="<<endl;
	cin>>b;
	cout<<"c="<<endl;
	cin>>c;

	cout<<"Valores iniciales: "<<endl;
	cout<<"x1: "<<x1<<endl;
	cout<<"x2: "<<x2<<endl;
	cout<<"a: "<<a<<endl;
	cout<<"b: "<<b<<endl;
	cout<<"c: "<<c<<endl;

	baskara(a,b,c,x1,x2);

	cout<<"Valores finales: "<<endl;
	cout<<"x1: "<<x1<<endl;
	cout<<"x2: "<<x2<<endl;
	cout<<"a: "<<a<<endl;
	cout<<"b: "<<b<<endl;
	cout<<"c: "<<c<<endl;



	return 0;
}


void mensajeBienvenida(){

cout<<"Bienvenido a la super calculadora de MATIAS"<<endl;
cout<<"Vamos a calcular las raices de una funcion cuadratica"<<
	endl<<"mediante el metodo de Baskara"<<endl<<endl;
}

void baskara(double a, double b, double c, double& x1, double& x2){

	double discriminante=pow(b,2)-4*a*c;
	cout<<"Discriminante: "<<discriminante<<endl;
	
	if(discriminante<0){
		cout<<"Raices complejas, no hago nada"<<endl;
	}
	if(discriminante>0){
		cout<<"Dos Raices reales"<<endl;
		discriminante=sqrt(discriminante);
		x1=(-b+discriminante)/(2*a);
		x2=(-b-discriminante)/(2*a);
	}
	if(discriminante==0){
		cout<<"Una raiz real doble"<<endl;
		discriminante=sqrt(discriminante);
		x1=(-b+discriminante)/(2*a);
		x2=x1;
	}


}