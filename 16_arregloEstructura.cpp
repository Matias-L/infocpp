#include <iostream>
#include <string>

using namespace std;
#define numPelis 2


/*

Estructura->Arreglo de estructuras->Manipulacion archivos

*/


int main(){

	/*
	struct pelicula{
		string titulo;
		int estreno;	//Año estreno
		float puntaje;
	};

	//Declaro variables;
	pelicula starWars;
	pelicula robocop;

	//Inicializo valores
	starWars.titulo="Star wars";
	starWars.estreno=1977;
	starWars.puntaje=4.5;

	robocop.titulo="Robocop";
	robocop.estreno=1987;
	robocop.puntaje=4.1;

	//Con arreglos

	pelicula coleccion[numPelis]={starWars, robocop};
*/

/*Forma alternativa*/

	struct pelicula{
		string titulo;
		int estreno;	//Año estreno
		float puntaje;
	} coleccion[numPelis];

	coleccion[0].titulo="Star wars";
	coleccion[0].estreno=1977;
	coleccion[0].puntaje=4.5;

	coleccion[1].titulo="Robocop";
	coleccion[1].estreno=1987;
	coleccion[1].puntaje=4.1;






	for(int i=0; i<numPelis;i++){
		cout<<coleccion[i].titulo<<endl;
		cout<<coleccion[i].estreno<<endl;
		cout<<coleccion[i].puntaje<<endl;
		cout<<endl;
	}


	


	return 0;
}