#include <iomanip>
#include <iostream>
#include <bitset>

/*
Programa para mostrar como representar los numeros en diferentes bases, el uso de "cin",
el modificador "showbase", sentencias IF-ELSE y SWITCH.

*/


using namespace std;

int main(){

 int numero = 0;
 int opcion=0;
 bool base=0;	//0:falso, todo lo demas verdadero 
 bool seguir=true;

 cout<<showbase; //Que siempre me muestre la base

 while(seguir){ //PREGUNTA: ¿conviene usar FOR aca?
		//¿Como haria lo mismo con DO-WHILE?¿Que cambiaria?
	 cout<<"Ingrese un número entero"<<endl;
	 cin>>numero;

	 cout<<"Ingrese la base en que desea mostrarlo"<<endl;
	 cout<<"1- Decimal"<<endl;
	 cout<<"2- Hexadecimal"<<endl;
	 cout<<"3- Octal"<<endl;
	 cout<<"4- Binario"<<endl;
	 cout<<"5- Salir"<<endl;
	 cin>>opcion;

		switch(opcion){

		case 1:
		 cout<<numero<<dec<<endl;
		break;

		case 2:
		 cout<<hex<<numero<<endl;
		break;

		case 3:
		 cout<<oct<<numero<<endl;
		break;
		
		case 4:
		 cout<<std::bitset<32>(numero)<<endl;
		break;
		
		case 5:
		 seguir=false;
		 cout<<"Adios"<<endl;
		break;

		default: cout<<"ERROR"<<endl<<endl;
		}
 }//Fin while


cin.get();
return 0;
}

