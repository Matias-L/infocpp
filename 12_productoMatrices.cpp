#include <iostream>

using namespace std;

/*

			matB[3][1]
			      ╔══╗
			      ║30║
			      ║20║
 			      ║10║
			      ╚══╝
		╔═════════╗ ╔════╗	
		║10 20 30 ║ ║1000║
matA[3][3]	║ 9  7  1 ║ ║ 420║ matC[3][1]
		║ 3 15  8 ║ ║ 470║
		╚═════════╝ ╚════╝

*/

int main(){
	int matA[3][3]={10,20,30,9,7,1,3,15,8};
	int matB[3]={30,20,10};
	int matC[3];

	for(int i=0; i<3;i++){		//Aca necesitamos un solo indice por la dimencion de matC
		matC[i]=matA[i][0]*matB[0]+matA[i][1]*matB[1]+matA[i][2]*matB[2];
		cout<<endl<<matC[i];
	}
	cout<<endl;




cin.get();


/*	
	MATRIZ[FILAS][COLUMNAS]

			matB[3][3]
			      ╔════════╗
			      ║ 2  7  3║
			      ║ 2 10 18║
 			      ║10  6 12║
			      ╚════════╝
		╔═════════╗ ╔═══════════╗	
		║ 1  2  3 ║ ║ 36  45  75║
matA[3][3]	║ 9  7  1 ║ ║ 42 139 165║ matC[3][3]
		║ 3 15  8 ║ ║116 219 375║
		╚═════════╝ ╚═══════════╝

*/


	int matrizA[3][3]={1,2,3,9,7,1,3,15,8};
	int matrizB[3][3]={2,7,3,2,10,18,10,6,12};
	int matrizC[3][3];

	for(int i=0; i<3;i++){		//i maneja filas de matrizC
		for(int j=0;j<3;j++){	//j maneja columnas de matrizC
			matrizC[i][j]=matrizA[i][0]*matrizB[0][j]+matrizA[i][1]*matrizB[1][j]+matrizA[i][2]*matrizB[2][j];
			cout<<" "<<matrizC[i][j];
		}
	cout<<endl;
	}
	cout<<endl;








return 0;
}