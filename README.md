Repositorio de los ejemplos mostrados en clases

Redictado informatica (C++) 2019

Autor: Ceballos, Matias Lionel

Link: https://bitbucket.org/Matias-L/infocpp/src/master/

Compilar y ejecutar en Linux:
Compilar:
g++ -o <nombreDeseado> <nombre codigo fuente>

Ejecutar:
./<nombreEjecutable>

En caso de que no se genere el archivo de salida como ejecutable automaticamente, correr:
chmod +x ./<nombreEjecutable>